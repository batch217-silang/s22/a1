console.log('Hello World');

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];


function addUser(userNameIput) {
    let validateUser = registeredUsers.some(function(userName) {
        return (userNameIput == userName);
    });
    if(validateUser) {
        alert("Registration failed. Username already exists!");
    }
    else {
        registeredUsers.push(userNameIput);
        alert("Thank you for registering!");
    }
}


function addUserFriend(userNameIput) {
    let validateUser = registeredUsers.some(function(userName) {
        return (userNameIput == userName);
    });
    if(validateUser) {
        friendsList.push(userNameIput);
        alert("You have added " + userNameIput + " as a friend!");
    }
    else {
        alert("User not found.");
    }
}


  function displayFriends() {
    if(friendsList.length <= 0 ) {
        alert("You currently have 0 friends. Add one first.");
    }
    else {
        friendsList.forEach(function(friend) {
            console.log(friend);
        })
    }
  }  



function countUserFriends() {
    if (friendsList.length <= 0) {
        alert("You currently have 0 friends. Add one first.");
    }
    else {
        alert("You currently have " + friendsList.length  +" friends")
    }
}


function deleteUserFriend() {
    if (friendsList.length <= 0) {
        alert("You currently have 0 friends. Add one first.");
    }
    else {
        friendsList.pop();
    }
}



 function deleteSpecificUserFriend(index) {
    if (friendsList.length <= 0) {
        alert("You currently have 0 friends. Add one first.");
    }
    else {
        friendsList.splice(index, 1);
    }
 }


